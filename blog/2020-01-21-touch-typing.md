---
title: Touch Typing Tutors and Games
date: 2020-01-21
tags: typing
type: note
---

A collection of links to touch typing tutors and games.

<!-- more -->


## Online Typing Tutors

*   [The Typing Cat](http://thetypingcat.com/ ) - Free online keyboard typing tutor
*   [TypingClub](https://www.typingclub.com/) - Learn Touch Typing Free
*   [typing.io](https://typing.io/) - Typing Practice for Programmers
*   [Typing Scout](http://typingscout.com/en/ ) - Faster typing means more spare time :)
*   [Solotyping](http://solotyping.com/?locale_id=2) - Touch Typing Course | Online Typing Test | Learn to Type Faster
*   [Ratatype](https://www.ratatype.com/) — Online typing tutor and typing lessons
*   [10FastFingers.com](https://10fastfingers.com/) - [Typing Test](https://10fastfingers.com/typing-test/english), Competitions, Practice & Typing Games
*   [TyperA](http://typera.net/) - the original typing test
*   [TypingStudy](https://www.typingstudy.com/) - Touch Typing Practice Online

## Software

*   [Fonttype](http://frontype.com/about_two.php) (windows) - Look front. Type looking front. Frontype!.
    *   Onscreen keyboard to learn typing (instead of tutors). Interesting idea, I didn't try this myself
*   [GNU Typist](http://www.gnu.org/software/gtypist/) (cross-platform) - typing tutor
*   The Typing of the Dead (windows game) - [Wikipedia](https://en.wikipedia.org/wiki/The_Typing_of_the_Dead?oldformat=true), [Steam](https://store.steampowered.com/app/246580/The_Typing_of_The_Dead_Overkill/)

## Touch Typing Games

*   [Nitro Type](https://www.nitrotype.com/) - Competitive Typing Game | Race Your Friends
*   [TypeRacer](https://play.typeracer.com/) - Free typing game and competition
*   [Клавогонки](http://klavogonki.ru/) - racing game
*   [ZType](https://zty.pe/) – Typing Game - Type to Shoot
*   [Hacker Typer](http://hackertyper.com/) - just start typing anything to look smart
*   [git-invaders](http://animuchan.net/git-invaders/) ★ GitHub Game Off 2012

## Keyboard Memo

Keyboard with finger positions (source: [http://frontype.com/keyboarding/400px-Touch-typing.png](http://frontype.com/keyboarding/400px-Touch-typing.png)):

![Keyboard Memo](media/400px-Touch-typing.png "Keyboard Touch Typing Memo")

Shows which key should be pressed with which finger (middle pink is for index fingers, green for middle fingers and so on).

Grey keys: I usually use pinky here, except for the space bar).

Space bar: Press with left thumb after typing a letter with right hand and press with the right thumb after typing a letter with left hand.
